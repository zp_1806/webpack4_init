import '@/scss/index.scss';

import { App } from '@/js/App';
import { Header } from '@/components/Header/index.js';
import { Footer } from '@/components/footer';

class Index extends App {

    constructor($, app) {
        super($, app);
        console.log(this);
    }

    render() {
        new Header(this.$app).init();
        
        new Footer(this.$app).init();
        $('body').prepend(this.$app)
    }
}
new Index(jQuery);