import '@/scss/index.scss';

import { App } from '@/js/App';
import { Header } from '@/components/Header/index.js';
import { Footer } from '@/components/footer';

class Detail extends App {

    constructor($, app) {
        super($, app);
        console.log(this);
    }

    render() {
        new Header(this.$app).init();
        
        $('body').prepend(this.$app);
    }
}
new Detail(jQuery);