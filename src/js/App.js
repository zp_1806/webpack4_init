class App {
    constructor($, app, options) {
        this.$app = $('<div id="app"></div>')
        this.init()
    }

    async init() {
        this.render();
    }

}

export { App };