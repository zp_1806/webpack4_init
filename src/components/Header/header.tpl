<header class="header">
    <div class="container">
        {{logo}}
        <div class="userinfo">
            <div>{{tel}}</div>
            <div>{{name}}</div>
        </div>
    </div>
</header>