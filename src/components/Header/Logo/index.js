import tpl from './logo.tpl';
class Logo {
    constructor() {
        this.name = 'logo';
        this.tpl = tpl;
    }
}
export { Logo };