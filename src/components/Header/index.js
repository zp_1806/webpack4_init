import tpl from './header.tpl';
import { Logo } from './Logo/index';
import tools from '@/utils/tools.js';
import './index.scss';
class Header {
    constructor(el) {
        this.$el = el;
        this.name = 'header';
        this.logo = new Logo();
    }

    async init() {
        await this.render();
    }

    async render() {
        await this.$el.append(tools.tplReplace(tpl(), {
            logo: this.logo.tpl(),
            name: '张鹏',
            tel: '18838702683'
        }))
    }
}

export { Header };