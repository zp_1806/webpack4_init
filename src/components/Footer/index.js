import tpl from './footer.tpl';
import tools from '@/utils/tools';
import './index.scss';
class Footer {
    constructor(el) {
        this.$el = el;
    }

    async init() {
        await this.render();
    }

    async render() {
        await this.$el.append(tools.tplReplace(tpl(), {
            content: '豫ICP备20015836号'
        }))
    }


}
export { Footer };